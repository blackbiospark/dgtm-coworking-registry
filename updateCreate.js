 const ApiEndpoint = "http://s28.ca/sodv1201/coworking/listing";
 // Build the API request for the test (what filters will we send with the request?).
 const ApiRequest = {
     ListingId: 0, // The listing id must be zero to create a new listing.
     PropertyId: 0, // The id of the property to which the listing belongs (required).
     ListingType: '', // The type of the new listing (required).
     Price: 0.00, // The daily rental cost of the new listing (required).
     Available: 0, // The availability of the new listing (required).
     Amenities: '', // The comma-delimited set of amenity codes for the new listing (required).
     Delete: 0 // This flag must be zero for a new listing. 
 }

 let amentities_list = [];
 let checked_amentities_list = [];
 let result_amentities = [];

 function showingAmenities() {
     console.log("heloo man");
     let AmenitiesEndpoint = "http://s28.ca/sodv1201/coworking/amenities";
     let S = fetch(AmenitiesEndpoint, {
         method: 'POST',
         body: JSON.stringify(ApiRequest)
     })
     S.then((ApiResponse) => {

             return (ApiResponse.json());
         })
         .then((Reply) => {

             Reply.Amenities.forEach(value => {

                 amentities_list.push(value);
                 $(".amenitiesList").append('<input type="checkbox" name="Amenities" value= "' + value.AmenityName + '">' + value.AmenityName + "<br>");

             });
         })

         .catch((e) => {
             console.error("Catch --> ");
             console.error(e);
         });
 }

 function updateListing() {

     /****************Setting the listingID **********/


     let listingID = $(".ListingID").val();

     if (listingID <= 0 || listingID === null || listingID === ' ') {
         $(".id").append('<div style="color:red;font-size:15px;">Please type valid value</div>');
         setTimeout(function () {
             location.reload(1);
         }, 2000);
     } else {
         listingID = Math.trunc(listingID);
         ApiRequest.ListingId = listingID;
         //console.log(ApiRequest.ListingId);

     }



     /***********Setting the property ID **********/
     let propertyID = $('.propertyID').val();
     if (propertyID <= 0 || propertyID === null || propertyID === ' ') {
         $(".property").append('<div style="color:red;font-size:15px;">Please type valid value</div>');
         setTimeout(function () {
             location.reload(1);
         }, 2000);

     }
     propertyID = Math.trunc(propertyID);
     ApiRequest.PropertyId = propertyID;
     //console.log(propertyID);


     /*******Listing type******/
     $("input:radio[name=listing_type]:checked").each(function (i) {

         if (i === 0) {
             ApiRequest.ListingType = $(this).val();
         } else {
             ApiRequest.ListingType = ApiRequest.ListingType + ', ' + $(this).val();
         }
     });

     if (ApiRequest.ListingType === '') {

         $(".space").append('<div style="color:red;font-size:15px;">Please type valid value</div>');
         setTimeout(function () {
             location.reload(1);
         }, 2000);

     }
     // console.log(ApiRequest.ListingType);

     /********Max_Price*********/
     let Max_price = $(".Max_Price").val();

     if (Max_price <= 0 || Max_price === null || Max_price === '') {
         $(".cover").append('<div style="color:red;font-size:15px;">Please type valid value</div>');
         setTimeout(function () {
             location.reload(1);
         }, 2000);


     } else {
         Max_price = Math.trunc(Max_price);
         ApiRequest.Price = Max_price;
         //  console.log(Max_price);

     }
     /*********Avaliable*********/

     $("input:checkbox[name=available]:checked").each(function (i) {

         if (i === 0) {
             ApiRequest.Available = $(this).val();
         }
     });

     // console.log(ApiRequest.Available);

     /**********Amentities********/


     checked_amentities_list = [];


     $("input:checkbox[name=Amenities]:checked").each(function (i) {



         checked_amentities_list.push($(this).val());

     });

     // console.log(checked_amentities_list);



     amentities_list.forEach((value, index) => {
         checked_amentities_list.forEach((val, ind) => {

             if (value.AmenityName === val) {

                 result_amentities.push(value.AmenityId);

             }



         }); // end of checked_amentities_list



     }); // end of amentities_list


     //console.log(result_amentities);


     result_amentities.forEach(function (value, index) {

         if (index === 0) {
             ApiRequest.Amenities = value;
         } else {
             ApiRequest.Amenities = ApiRequest.Amenities + ', ' + value;

         }


     }); // end of result_amentities
     //  console.log(ApiRequest.Amenities);
     console.log(ApiRequest)

     // Send a POST request to the API endpoint, including the API request data.
     let P = fetch(ApiEndpoint, {
         method: 'POST',
         body: JSON.stringify(ApiRequest)
     });

     // When the promise resolves, convert the HTTP reply from JSON to a JS object and log it.
     P.then((ApiResponse) => {

             return (ApiResponse.json());
         })
         .then((Reply) => {
             console.log(Reply);
             if (Reply.Status === "success") {
                 window.alert("Listing is updated successfully");
             } else {
                 window.alert("please type is valid input");
             }
         })
         .catch((e) => {
             console.error("Catch --> ");
             console.error(e);
         });



 }

 /************************************************************
  **************END OF FUNCTION UPDATE LISTING*****************
  *************************************************************/


 function createListing() {
     /****************Setting the listingID **********/


     ApiRequest.ListingId = 0;
     console.log(ApiRequest.ListingId);

     /***********Setting the property ID **********/
     let propertyID = $('.propertyID').val();
     if (propertyID <= 0 || propertyID === null || propertyID === ' ') {
         $(".property").append('<div style="color:red;font-size:15px;">Please type valid value</div>');
         setTimeout(function () {
             location.reload(1);
         }, 2000);

     }
     propertyID = Math.trunc(propertyID);
     ApiRequest.PropertyId = propertyID;
     console.log(propertyID);


     /*******Listing type******/
     $("input:radio[name=listing_type]:checked").each(function (i) {

         if (i === 0) {
             ApiRequest.ListingType = $(this).val();
         } else {
             ApiRequest.ListingType = ApiRequest.ListingType + ', ' + $(this).val();

         }
     });

     if (ApiRequest.ListingType === '') {

         $(".space").append('<div style="color:red;font-size:15px;">Please type valid value</div>');
         setTimeout(function () {
             location.reload(1);
         }, 2000);

     }
     console.log(ApiRequest.ListingType);

     /********Max_Price*********/
     let Max_price = $(".Max_Price").val();

     if (Max_price <= 0 || Max_price === null || Max_price === '') {
         $(".cover").append('<div style="color:red;font-size:15px;">Please type valid value</div>');
         setTimeout(function () {
             location.reload(1);
         }, 2000);


     } else {
         Max_price = Math.trunc(Max_price);
         ApiRequest.Price = Max_price;
         console.log(Max_price);

     }
     /*********Avaliable*********/

     $("input:checkbox[name=available]:checked").each(function (i) {

         if (i === 0) {
             ApiRequest.Available = $(this).val();
         }
     });

     console.log(ApiRequest.Available);

     /**********Amentities********/


     


     $("input:checkbox[name=Amenities]:checked").each(function (i) {



         checked_amentities_list.push($(this).val());

     });

     console.log(checked_amentities_list);



     amentities_list.forEach((value, index) => {
         checked_amentities_list.forEach((val, ind) => {

             if (value.AmenityName === val) {

                 result_amentities.push(value.AmenityId);

             }



         }); // end of checked_amentities_list



     }); // end of amentities_list


     console.log(result_amentities);


     result_amentities.forEach(function (value, index) {

         if (index === 0) {
             ApiRequest.Amenities = value;
         } else {
             ApiRequest.Amenities = ApiRequest.Amenities + ', ' + value;

         }


     }); // end of result_amentities
     console.log(ApiRequest.Amenities);
     console.log(ApiRequest);


     // Send a POST request to the API endpoint, including the API request data.
     let P = fetch(ApiEndpoint, {
         method: 'POST',
         body: JSON.stringify(ApiRequest)
     });

     // When the promise resolves, convert the HTTP reply from JSON to a JS object and log it.
     P.then((ApiResponse) => {

             return (ApiResponse.json());
         })
         .then((Reply) => {

             console.log(Reply);
             if (Reply.Status === "success") {
                 window.alert("Listing is created successfully");
             } else {
                 window.alert("please type is valid input");
             }
         })
         .catch((e) => {
             console.error("Catch --> ");
             console.error(e);
         });

 }
 /************************************************************
  **************END OF FUNCTION UPDATE LISTING*****************
  *************************************************************/
