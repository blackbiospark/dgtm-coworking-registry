//USE THIS FOR SCRIPTS ON THE FIND LISTINGS PAGE
"use strict"

var ListingInfo =
{
	ListingId: 0,	// If nonzero, filters results to a specific listing.
	OwnerId: 0,		// If nonzero, filters results to listings for a specific property owner.
	CityName:'',		// If nonblank, filters results to listings in the matching city.
	ListingType:'',	// If nonblank, filters results to listings that match the type.
	MaxPrice:0.00,	// If nonzero, filters listings to those under this price target.
	Available:0,		// If nonzero, filters listings to available only.
	Amenities:''		// Contains a comma-separated list of amenity codes.
}
//Object for containing the data of the search

const Endpoint = "http://s28.ca/sodv1201/coworking/listings";
//This will be updated when the API is finished

var validationValue = 0;
//A number for validation reasons

var AmenStorage = [];
//To store amenity objects. Should scale well since there will never be an absurd amount of amenities

const searchMin = 0;
var searchMax;
//Used for pagination, used to be able to keep track of the amount of results

var currentMin = 0;
var currentMax = 5;
//Used for pagination, used to show the current range of searches

var Sort = '';
//Used for search filter

var map;
function initMap()
{
    map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: 51.0486, lng: -114.0708},
        zoom: 12
    });
}
//The map

var marker;
//Used for creating markers on the map

var markers = [];
//The list of markers on the map

function ClearMarkerHelper(map)
{
    markers.forEach(marker =>
    {
        marker.setMap(map);
    })
}
//Helps clear markers

function TrueMarkerHelper()
{
    ClearMarkerHelper(null);
    markers = [];
}
//Helps clear markers

function GetAmenities()
{
    let arr = [];
    
    let url = 'http://s28.ca/sodv1201/coworking/amenities';
    
    let P = fetch(url, {method: 'POST'});
    
    P.then(Response => Response.json())
     .then(Reply =>
    {
        Reply.Amenities.forEach(amen =>
        {
            $('.amenities').append('<div><input type="checkbox" name="amenities" value="' + amen.AmenityId + '">' + amen.AmenityName + '</div>');
            AmenStorage.push(amen);
        });
    });
};
//Gets the list of the amenities via an API call, creates checkboxes for each one, and stores the amenity objects locally

$(".searchListings").click(function()
{
    validationValue = 0;
    //Resets the previously decalred number once the function is called, in case it gets changed
    
    currentMin = 0;
    currentMax = 5;
    //Resets the range of the search with each new search

    var name = $(".ListingName").val();
    //Gets the building name, if it exists
    
    var lid = Math.trunc($('.ListingID').val());
    
    if(lid === '')
    {
        $('.invalidLID').empty();
        lid = 0;
    }
    else if(lid < 0)
    {
        $(".invalidLID").empty();
        $(".id").append("<div class='invalidLID'>The listing ID you have entered is invalid, please enter a listing ID that is a whole, positive number, or leave the field blank.</div>")
        lid = 0;
        $(".invalidLID").css("color", "red");
        validationValue = 1;
    }
    if(lid >= 0)
    {
        $('.invalidLID').empty();
    }
    //Gets a listing ID, if it exists. Uses truncation since no listing ID exists as a decimal, and notifys the user of an invalid listing ID if they input a negative one since there are no negative listing IDs
    
    
    
    var id = Math.trunc($('.OwnerID').val());
    
    if(id === '')
    {
        $('.invalidID').empty();
        id = 0;
    }
    else if(id < 0)
    {
        $(".invalidID").empty();
        $(".owner").append("<div class='invalidID'>The user ID you have entered is invalid, please enter a user ID that is a whole, positive number, or leave the field blank.</div>")
        id = 0;
        $(".invalidID").css("color", "red");
        validationValue = 1;
    }
    if(id >= 0)
    {
        $('.invalidID').empty();
    }
    //Gets a owner ID, if it exists. Uses truncation since no owner ID exists as a decimal, and notifys the user of an invalid owner ID if they input a negative one since there are no negative owner IDs

    ListingInfo.Amenities = '';

    var amenities = $("input:checkbox[name=amenities]:checked").each(function(i)
    {
        if(i === 0)
        {
            ListingInfo.Amenities += $(this).val();
        }

        else
        {
            ListingInfo.Amenities +=  ', ' + $(this).val();
        }
            
    });
    //Loops through the amenities checkboxes and retrieves the values. Separates each one with commas and spaces
    
    
    
    ListingInfo.ListingType = '';

    var spaceType = $("input:checkbox[name=spaceType]:checked").each(function(i)
    {
        if(i === 0)
        {
            ListingInfo.ListingType += $(this).val();
        }

        else
        {
            ListingInfo.ListingType +=  ', ' + $(this).val();
        }    
    });
    //Loops through the space type checkboxes and retrieves the values. Like amenities, they are separated by commas and spaces
    
    
    
    ListingInfo.Available = 0;
    var avail = $('input:checkbox[name=avail]:checked').each(function()
    {
        ListingInfo.Available = $(this).val();
    });

    var price = $(".ListingPrice").val();
    if (price === "")
    {
        $(".invalidPrice").empty();
        price = 0.00;
    }
    else if (price < 0.00) 
    {
        $(".invalidPrice").empty();
        $(".price").append("<div class='invalidPrice'>The price you have entered is invalid, please enter a price that is a positive number</div>")
        price = 0.00;
        $(".invalidPrice").css("color", "red");
        validationValue = 1;
    } 
    else if (price >= 0)
    {
        $(".invalidPrice").empty();
    }
    //Get the inputted maximum price. Notifies the user if they put in a price below 0, and tells them to put in either a price that is >0, or leave the field blank
    
    Sort = $('input[name="order"]:checked').val();
    if(Sort != 'distance' && Sort != 'price')
        Sort = '';
    //Finds out how you want to sort the results by
    
    ListingInfo.CityName = name;
    ListingInfo.MaxPrice = price;
    ListingInfo.OwnerId = id;
    ListingInfo.ListingId = lid;
    //Sets the values of the object
                
    if (validationValue > 0)
    {
        alert("Search not sent, please ensure all fields are valid before searching.");
    }
    //Alerts the user if any fields are filled with invalid data, and doesn't send a search request, but if there are no errors, the search continues
    
    else
    {        
        const bodyData = ListingInfo;
        //Makes the object to send out

        let P = fetch(Endpoint, {
            method: "POST",
            body: JSON.stringify(bodyData)
        });
        //Makes the fetch request and begins with promises

        P.then((Response) => Response.json())
        //First response
        .then((Reply) =>
        {
            currentMin = 0;
            currentMax = 5;
            //Resets the search range on each now search, for extra safety
            $('.showResults').empty();
            //Clears all previous results being shown
            searchMax = Reply.Listings.length;
            //Stores the max amount of results
            if(Sort === 'distance')
            {
                Reply.Listings.sort(function(a, b)
                {
                   return distanceCalc(a.Latitude, a.Longitude) - distanceCalc(b.Latitude, b.Longitude);
                });
            }
            //Sorts by distance with math
            else if (Sort === 'price')
            {
                Reply.Listings.sort(function(a, b)
                {
                    return a.Price - b.Price;
                });
            };
            //Sorts by price from lowest to highest
            TrueMarkerHelper();
            //Clears all markers on the map
            Reply.Listings.slice(currentMin, currentMax).forEach((Result, Index) =>
            {
                $('.showResults').append("<ul class='result" + Index + "'></ul>");
                $('.result' + Index).append("<li>Name of building: " + Result.BuildingName + "</li>");
                $('.result' + Index).append("<li>Location: " + Result.CityName + "</li>");
                $('.result' + Index).append("<li>Listing type: " + Result.ListingType + "</li>");
                $('.result' + Index).append("<li>Price per day in CAD: " + Result.Price + "</li>");
                $('.result' + Index).append("<li>Neighborhood: " + Result.NeighborhoodName + "</li>");
                $('.result' + Index).append("<li>Amenities: " + AmenityNames(Result.Amenities) + "</li>");
                $('.result' + Index).append("<li>Distance: " + distanceCalc(Result.Latitude, Result.Longitude) + " kilometres</li>");
                $('.result' + Index).append("<li>Availability: " + availConvert(Result.Available) + "</li>");
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(Result.Latitude, Result.Longitude),
                    map: map,
                    title: Result.BuildingName
                });
                markers.push(marker);
            });
            //Gets up to 5 results at a time, and shows some info about the listing, as well as putting a marker with its location on the map
            if (searchMax >= 5)
                $('.showResults').append('<button onclick="nextFive()">View up to the next 5 listings</button>');
            //If there are more than the 5 results on the page, then it appends a button to show up to the next 5 results
            
            alert("Search successfully sent!");
            //Tells the user that their search went off
        })
        .catch((error) =>
        {
            alert("There was an error with your search request, please try again");    
        });
        //Catch for failed API requests
    };
});

function AmenityNames(str)
{    
    let returnArr = [];
    let uniqueArr = [];
    let returnStr = '';
    let arr = str.split(', ');
    
    arr.forEach(a =>
    {
        AmenStorage.forEach(b =>
        {
            if(a == b.AmenityId)
            {
                a = b.AmenityName;
                returnArr.push(a);
            };
        });
    });
    
    $.each(returnArr, (i, el) =>
    {
       if($.inArray(el, uniqueArr) === -1)
           uniqueArr.push(el);
    });
    
    uniqueArr.forEach((el, i) =>
    {
        if(i === 0)
            returnStr += el;
        else
            returnStr += ', ' + el;
    });
    
    return returnStr;
};
//Converts amenity IDs into names, separated by commas and spaces

function availConvert(bool)
{
    if(bool === 0)
        return 'Unavailable';
    else
        return 'Available';
};
//Converts the bool representing availabiity into something more user friendly

function toRadians(int)
{
    return int * Math.PI / 180;
};
//Converts a number to radians

function distanceCalc(inlat, inlong)
{
        const lat = 51.0486;
        const long = -114.0708;
    
        var R = 6371 * Math.pow(10, 3);
        var lat1 = toRadians(lat);
        var lat2 = toRadians(inlat);
        var dlat = toRadians(inlat-lat);
        var dlong = toRadians(inlong-long);

        var a = Math.sin(dlat/2) * Math.sin(dlat/2) +
            Math.cos(lat1) * Math.cos(lat2) *
            Math.sin(dlong/2) * Math.sin(dlong/2);
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));

        var d = (R * c) / 1000;
            
        return Math.round(d * 1000) / 10000;
}
//Function used to calculate distance, used exclusively in the distance search filter

function nextFive()
{      
    currentMax += 5;
    currentMin += 5;
    //Increases the current range by 5 on both ends
    
    if (currentMax === searchMax)
        currentMin -= 5;
    if (currentMax > searchMax)
        currentMax = searchMax
    //Checks for if the current max exceeds the max results in the search, and corrects if if it does exceed that number
        
    $('.showResults').empty();
    //Clears the displayed results before showing new ones
    
    const bodyData = ListingInfo;

    let P = fetch(Endpoint, {
        method: "POST",
        body: JSON.stringify(bodyData)
    });
    
    P.then((Response) => Response.json())
    .then((Reply) =>
    {
        $('.showResults').empty();
        searchMax = Reply.Listings.length;
        if(Sort === 'distance')
        {
            Reply.Listings.sort(function(a, b)
            {
               return distanceCalc(a.Latitude, a.Longitude) - distanceCalc(b.Latitude, b.Longitude);
            });
        }
        else if (Sort === 'price')
        {
            Reply.Listings.sort(function(a, b)
            {
                return a.Price - b.Price;
            });
        };
        TrueMarkerHelper();
        Reply.Listings.slice(currentMin, currentMax).forEach((Result, Index) =>
        {
            $('.showResults').append("<ul class='result" + Index + "'></ul>");
            $('.result' + Index).append("<li>Name of building: " + Result.BuildingName + "</li>");
            $('.result' + Index).append("<li>Location: " + Result.CityName + "</li>");
            $('.result' + Index).append("<li>Listing type: " + Result.ListingType + "</li>");
            $('.result' + Index).append("<li>Price per day in CAD: " + Result.Price + "</li>");
            $('.result' + Index).append("<li>Neighborhood: " + Result.NeighborhoodName + "</li>");
            $('.result' + Index).append("<li>Amenities: " + AmenityNames(Result.Amenities) + "</li>");
            $('.result' + Index).append("<li>Distance: " + distanceCalc(Result.Latitude, Result.Longitude) + " kilometres</li>");
            $('.result' + Index).append("<li>Availability: " + availConvert(Result.Available) + "</li>");
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(Result.Latitude, Result.Longitude),
                map: map,
                title: Result.BuildingName
            });
            markers.push(marker);
        });
        //Up to here, this is practically the same function as the intial search
        $('.showResults').append('<button onclick="prevFive()">View the previous 5 listings</button>');
        //Since you were able to view up to the next 5 results, there must be a previous 5
        if(currentMax != searchMax)
            $('.showResults').append('<button onclick="nextFive()">View up to the next 5 listings</button>');
        //If you are not yet at the end of the search results, allows you to view up to 5 more
    })
    .catch((error) => 
    {
        alert("There was an error with your search request, please try again");    
    });
    //Catch for failed API requests
};

function prevFive()
{
    if (currentMax === searchMax)
        currentMax = currentMin + 5;
    currentMin -= 5; 
    currentMax -= 5;
    //Sets the new range, taking into account if you were at the end of your search
        
    
    $('.showResults').empty();
    
    const bodyData = ListingInfo;

    let P = fetch(Endpoint, {
        method: "POST",
        body: JSON.stringify(bodyData)
    });
    
    P.then((Response) => Response.json())
    .then((Reply) =>
    {
        $('.showResults').empty();
        searchMax = Reply.Listings.length;
        if(Sort === 'distance')
        {
            Reply.Listings.sort(function(a, b)
            {
               return distanceCalc(a.Latitude, a.Longitude) - distanceCalc(b.Latitude, b.Longitude);
            });
        }
        else if (Sort === 'price')
        {
            Reply.Listings.sort(function(a, b)
            {
                return a.Price - b.Price;
            });
        };
        TrueMarkerHelper();
        Reply.Listings.slice(currentMin, currentMax).forEach((Result, Index) =>
        {
            $('.showResults').append("<ul class='result" + Index + "'></ul>");
            $('.result' + Index).append("<li>Name of building: " + Result.BuildingName + "</li>");
            $('.result' + Index).append("<li>Location: " + Result.CityName + "</li>");
            $('.result' + Index).append("<li>Listing type: " + Result.ListingType + "</li>");
            $('.result' + Index).append("<li>Price per day in CAD: " + Result.Price + "</li>");
            $('.result' + Index).append("<li>Neighborhood: " + Result.NeighborhoodName + "</li>");
            $('.result' + Index).append("<li>Amenities: " + AmenityNames(Result.Amenities) + "</li>");
            $('.result' + Index).append("<li>Distance: " + distanceCalc(Result.Latitude, Result.Longitude) + " kilometres</li>");
            $('.result' + Index).append("<li>Availability: " + availConvert(Result.Available) + "</li>");
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(Result.Latitude, Result.Longitude),
                map: map,
                title: Result.BuildingName
            });
            markers.push(marker);
        });
        //Up to here, this is practically the same function as the intial search
        if (currentMin === 0)
            $('.showResults').append('<button onclick="prevFive()">Previous 5 listings</button>');
        //If there are more searches that you have passed, creates a button that lets you view the previous 5 results
        if (currentMax != searchMax)
        $('.showResults').append('<button onclick="nextFive()">View up to the next 5 listings</button>');
        //If you were able to view the previous
    })
    .catch((error) =>
    {
        alert("There was an error with your search request, please try again");    
    });
    //Catch for failed API requests
};