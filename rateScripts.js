'use strict'

const Endpoint = 'http://s28.ca/sodv1201/coworking/rating';

var RateObject = {
	PropertyId: 0,	// The property id of an existing property to be rated (required).
	Rating:0			// The rating from 1 to 5 (required).
};
//Object containing data for the request

var validationValue = 0;
//Number for vaildation reasons

$('.rate').click(function()
{
    validationValue = 0;
    //Resets the validation value
    
    RateObject.PropertyId = Math.trunc($('.pid').val());
    //Gets the ID of the property to be rated
        
    if($('.pid').val() === '' || Math.trunc($('.pid').val() <= 0))
    {
        $('.noId').empty();
        $('.propId').append("<div class='noId'>The property ID you have entered was invalid. Please enter a property ID greater than 0.</div>");
        validationValue = 1;
        $('.noId').css('color', 'red');    
    }
    else if(Math.trunc($('.pid').val()) > 0)
    {
        $('.noId').empty();
    };
    //Checks for invalid values, such as an empty field, a negative, or a zero, and notifies the user if they do have an invalid value
    
    RateObject.Rating = 0;
    RateObject.Rating = $("input[name='rating']:checked").val();
    if(!RateObject.Rating ||RateObject.Rating === 0)
    {
        $('.noRate').empty();
        $('.rateOptions').append("<div class='noRate'>Please select a rating from the available options</div>");
        RateObject.Rating = 0;
        validationValue = 1;
        $('.noRate').css('color','red');
    }
    else
    {
        $('.noRate').empty();
    };
    //Checks if the user slected a rating from 1 to 5, notifies them if the did not select an option
    
    if (validationValue > 0)
        alert("Data not sent, please ensure all fields are filled before sending");
    //Alerts user if the data was unable to be sent due to invalid field data
        
    else
    {        
        const bodyData = RateObject;

        let P = fetch(Endpoint, {
            method: "POST",
            body: JSON.stringify(bodyData)
        });
            P.then((Response) => Response.json())
            .then((Reply) => {
            console.log(Reply);
            alert("Data successfully sent! If there is a property associated with that ID, it will be rated based on your input.");
            //Sends out the request if there were no errors in the fields
            })
            .catch((error) =>
            {
                alert("Your rating was unable to be sent, please try again later.");
            });
    };
});